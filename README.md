# AmiYuy's WeakAuras

Comprehensive auras for all hunter specs kept up to date for each patch as well as proc/buff auras for Heroism/Bloodlust, all Legion potions, all multi-class legendaries, and engineering devices. 

- [Hunter Auras](http://amiyuy.com/wow/2018/07/weakauras-2-exports-for-hunters-battle-for-azeroth-and-patch-8-0/)
- [All Classes and Engineers](http://amiyuy.com/wow/2017/05/weakauras-2-exports-for-all-classes-and-engineers-legion/)
- [wago.io Profile](https://wago.io/p/AmiYuy)
- [Issues](https://gitlab.com/amiyuy/WeakAuras/issues)

![Auras](http://amiyuy.com/wow/wp-content/uploads/2018/07/2018-07-15_Hunter-All2.jpg)